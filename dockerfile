FROM keymetrics/pm2:14-alpine

ENV NODE_ENV=production

COPY package.json .
COPY pm2.json .


COPY . .
RUN npm config set https-proxy http://proxy2.bri.co.id:1707
RUN npm config set proxy http://proxy2.bri.co.id:1707

RUN npm i
RUN npm i cross-env --save
RUN npm i body-parser --save
# RUN pm2 install pm2-server-monit

RUN npm config rm proxy
RUN npm config rm https-proxy

EXPOSE 3000

CMD pm2-runtime pm2.json